// let obj2 = new Object();
//
// function CreateObject(name, age) {
//     this.name = name;
//     this.age = age;
// }
//
// let obj3 = new CreateObject('Alex', 17);
//
// console.log(obj3);
//
// class People {
//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//     }
// }
//
// let person = new People('Vasya', 67);
//
// console.log(person);


// let obj = {
//     name: 'Alex',
//     age: 17,
//     permission: true,
//     index: 0,
//     showProp(callback) {
//         return callback(this)
//     }
// };

// obj.name = "Ben";
// console.log(obj.name);
// let a = obj.showProp(obj => obj.name = 'New Name');
// console.log(a)


let obj = {
    name: 'Alex',
    age: 17,
    permission: true,
    index: 0,
    showProp:() => this
};

console.log(obj.showProp());