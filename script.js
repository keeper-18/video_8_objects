/**
 * Object definition
 * */
let obj2 = {
    name: 'Alex',
    age: 17,
    index: 0,
};

/**
 * how to call
 * */
console.log(obj2.name)
console.log(obj2["name"])

/**
 * object reference
 * */

let obj1 = obj2;
delete obj1.name;
console.log(obj2);

/**
 * const allows you to change a property value
 * */

const man = {
    name: 'Some name',
    age: 16
};
man.name = "Albert";
console.log(man.name);

/**
 * приколы с переменной в качестве имени свойства
 * */

function name() {
    return 'fruit'
}

let obj4 = {
    [name()]: 'apple',
};

console.log(obj4.fruit);


/**
 * Object.assign - clone and merge
 * */

let user = {
    name: 'Alex'
}

let rights = {
    edit: true,
    create: false
}

Object.assign(user, rights);

console.log('user:', user);

/**
 * create clone
 * */

let clone = Object.assign({}, user, rights);
console.log('clone: ', clone);

/**
 * Object.create
 * */

var test = {
    val: 1,
    func: function() {
        return this.val;
    }
};
var testA = Object.create(test);

console.log('testA', testA);


/**
 * counter and function scope
 * */

let obj3 = {
    name: 'Alex',
    age: 17,
    index: 0,
    changeName: function () {
        return function() {
            return this.index++
        }.bind(this) // Ошибка, если не привяжем область видимости
    }
};

let res = obj3.changeName();
console.log(res());
console.log(res());


/**
 * *************
 * */
//
// let obj = {
//     name: 'Alex',
//     age: 17,
//     index: 0,
//     changeName: function (i, callback) {
//         callback(i, this)
//     }
// };
//
// for(let i = 1; i < 2; i++) {
//     obj.changeName(i, function(val, obj){
//         setTimeout(function() {
//             console.log(obj.name, obj.index++)
//         }, i * 1000)
//     });
// }

/**
 * different syntax of method
 * */
let obj = {
    name: 'Alex',
    age: 17,
    index: 0,
    changeName (i, callback) {
        callback(i, this)
    }
};

for(let i = 1; i <= 2; i++) {
    obj.changeName(i, function(val, obj){
        setTimeout(function() {
            console.log(obj.name, obj.index++)
        }, i * 1000)
    });
}
